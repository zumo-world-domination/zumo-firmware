#include "Governor.h"
#include <Arduino.h>
#ifdef GOVERNOR_EXTERNAL
uint16_t currentMaxSpeed = 200;

enum ZumoCommand currentObjective = STOP;
enum ZumoCommand lastObjective = STOP;


enum ZumoCommand getNewObjective(){
  while(Serial1.available() >= 5){
    int c;
    c = Serial1.read();
    if(c!='Z') continue;
    c = Serial1.read();
    if(c!='U') continue;
    c = Serial1.read();
    if(c!='M') continue;
    c = Serial1.read();
    if(c!='O') continue;
    c = Serial1.read();
    if(c == STOP || c == GO_NORTH || c == GO_SOUTH  || c == GO_EAST || c == GO_WEST || c == ZUMO_RESET){
      enum ZumoCommand newCommand = (enum ZumoCommand) c;
      return c;
    }else{
      return STOP;
    }
  }
  return STOP;
}
#endif
