#include "CircularBuffer.h"

void CircularBuffer::flush(){
  head = 0;
  tail = 0;
}

CircularBuffer::CircularBuffer(void *memory, uint8_t length){
  size = length;
  data = (uint8_t*) memory;
  flush();
}
bool CircularBuffer::isFull(){
  return (((head+1) == (tail)) || ((head == (size -1)) && ((tail == 0 ) )));
}

bool CircularBuffer::isEmpty(){
  return head == tail;
}


int CircularBuffer::get(){
  if(head == tail){
    return EOF;
  }else{
    uint8_t val = data[tail];
    tail = (tail == (size -1 )?0:(tail+1));
    return val;
  }
}

uint8_t CircularBuffer::put(uint8_t byte){
  if(isFull()){
    return 1;
  }else{
    data[head] = byte;
    head = (head == (size -1 )?0:(head+1));
    return 0;
  }
}
