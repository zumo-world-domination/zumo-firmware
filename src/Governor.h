#ifndef GOVERNOR_H
#define GOVERNOR_H
#include <stdint.h>
#include "ZumoProtocol.h"
//#define GOVERNOR_DUMMY
//#define GOVERNOR_CIRCLE
//#define GOVERNOR_RANDOM
#define GOVERNOR_EXTERNAL
//#define GOVERNOR_BACK_FORTH

extern uint16_t maxSpeed;
extern uint16_t approachMaxSpeed;
extern uint16_t currentMaxSpeed;
extern enum ZumoCommand currentObjective;

enum ZumoCommand getNewObjective();

#endif
