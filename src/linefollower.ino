/* This example is in large taken from the example provided
    when installing the ZUMO library.

    This demo requires a Zumo 32U4 Front Sensor Array to be
    connected, and jumpers on the front sensor array must be
    installed in order to connect pin 4 to DN4 and pin 20 to DN2. */

#include <Wire.h> 
#include <Zumo32U4.h> 
#include "Control.h" 
#include "Regulation.h" 
#include "Blackbox.h" 
#include "Hardware.h" 
#include "SerialNumber.h" 
#include <EEPROM.h> 
#include "EEPROM_map.h" 
#include "Governor.h" 
#include "DummyPrint.h" 



//extern movePatternState moveState; 
DummyPrint daveNull = DummyPrint(); 
Blackbox blackbox = Blackbox(Serial1);

void selectLogger(){ 
#if BLACKBOX_DEVICE==BLACKBOX_ASK 
#ifdef LCD
   lcd.clear();
   lcd.print(F("A:USB"));
   lcd.gotoXY(0, 1);
   lcd.print(F("B:UART")); 
#endif   
  bool buttonPressed = false;
  do{     
    if(buttonA.getSingleDebouncedPress()){ 
#ifdef LCD       
      lcd.clear();
      lcd.print(F("Plz open"));
      lcd.gotoXY(0, 1);
      lcd.print(F("USB serial"));
#endif
      Serial.begin(115200);
      while(!Serial);
      blackbox.changeOutput(Serial);
      buttonPressed=true;
    }     
    if(buttonB.getSingleDebouncedPress()){
      Serial1.begin(115200);
      blackbox.changeOutput(Serial1);
      buttonPressed=true;
    }
    /*  if(buttonC.getSingleDebouncedPress()){
        static Blackbox statBlackbox=Blackbox(daveNull);
        blackbox.changeOutput(daveNull); 
        buttonPressed=true;
        }*/
  }while(!buttonPressed);
#elif BLACKBOX_DEVICE==BLACKBOX_SERIAL
  Serial1.begin(115200);
  blackbox.changeOutput(Serial1);
#elif BLACKBOX_DEVICE==BLACKBOX_USB 
#ifdef LCD
  lcd.clear();
  lcd.print(F("Plz open"));
  lcd.gotoXY(0, 1);
  lcd.print(F("USB serial"));
#endif   
  Serial.begin(115200);
  while(!Serial);
  blackbox.changeOutput(Serial);
#error "Invalid Blackbox device. Check Blackbox.h"
#endif
} 

void setup() 
{   
  
  // Initialize randomness for the sake of random behaviour.
  srand(time(NULL));//TODO is this actually random in any way?
  Serial1.begin(115200);
  Serial.begin(115200);
  ledRed(1);
  Z:
  while(Serial1.read()!='Z'){
  }
  int c;
  do{
    c = Serial1.read();
  }while(c == EOF);
  if(c!='U') goto Z;
  do{
    c = Serial1.read();
  }while(c == EOF);
  if(c!='M') goto Z;
  do{
    c = Serial1.read();
  }while(c == EOF);
  if(c!='O') goto Z;
  do{
    c = Serial1.read();
  }while(c == EOF);
  if(c!='I') goto Z;
  do{
    c = Serial1.read();
  }while(c == EOF);
  if(c!='N') goto Z;
  do{
    c = Serial1.read();
  }while(c == EOF);
  if(c!='I') goto Z;
  do{
    c = Serial1.read();
  }while(c == EOF);
  if(c!='T') goto Z;
  Serial.println("init");
  ledRed(0);
  initSensors();
#ifdef LCD
  lcd.clear();
#endif
  selectLogger();
#ifdef LCD
  lcd.print(readSerialNumber(), HEX);
  lcd.gotoXY(0,1);
  lcd.print(readBootCount());
#endif
  Serial1.print("RDY");
  Serial.println("rdy");
  blackbox.begin();
  //currentObjective = STOP;
  ledGreen(1);
} 

void loop() 
{
  updateSensors();
  if(currentObjective == STOP){
    currentObjective = getNewObjective();
  }   

  resetRegulatorOutput(); 
  if(control() && currentObjective != STOP){//current objective achieved
    blackbox.event(COMMAND_COMPLETE);
    currentObjective = STOP;
  } 
  /*if(regulatorOutput.leftCmd > 10 && regulatorOutput.leftCmd < 60){
    regulatorOutput.leftCmd = 60;
  }
  if(regulatorOutput.leftCmd < -10 && regulatorOutput.leftCmd > -60){
    regulatorOutput.leftCmd = -60;
  }
  if(regulatorOutput.rightCmd > 10 && regulatorOutput.rightCmd < 60){
    regulatorOutput.rightCmd = 60;
  }
  if(regulatorOutput.rightCmd < -10 && regulatorOutput.rightCmd > -60){
    regulatorOutput.rightCmd = -60;
  } */
  motors.setSpeeds(regulatorOutput.leftCmd,regulatorOutput.rightCmd);
  
  blackbox.log();
}

