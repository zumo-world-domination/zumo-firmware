#include "Hardware.h"

Zumo32U4LineSensors lineSensors;
Zumo32U4ButtonA buttonA;
Zumo32U4ButtonB buttonB;
Zumo32U4ButtonC buttonC;
Zumo32U4LCD lcd;
L3G gyro;
LSM303 compass;
Zumo32U4Motors motors;
Zumo32U4ProximitySensors proxSensors;
Zumo32U4Encoders encoders;
