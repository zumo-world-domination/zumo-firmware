#include <avr/boot.h>
#include "EEPROM_map.h"
#include <EEPROM.h>
bool bootCountUpdated = false;

uint16_t readSerialNumber(){
  uint16_t number=0;

  *(((uint8_t*)&number)+1) = boot_signature_byte_get(22);
  *((uint8_t*)&number) = boot_signature_byte_get(23);
  return number;
}
uint16_t bootCount;
uint16_t readBootCount(){
  EEPROM.get(EEP_BOOTCOUNT_ADDRESS, bootCount);
  if(!bootCountUpdated){
    bootCount += 1;
    EEPROM.put(EEP_BOOTCOUNT_ADDRESS, bootCount);
    bootCountUpdated=true;
  }

  return bootCount;
}
