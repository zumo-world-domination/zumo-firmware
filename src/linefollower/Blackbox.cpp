#include "Blackbox.h"
#include <Arduino.h>
#include <Zumo32U4.h>
#include "Sensors.h"
#include "Regulation.h"
#include "SerialNumber.h"
#include "Control.h"


int getLeftMotorSpeed(){
  return digitalRead(16) ? OCR1B * -1 : OCR1B;
}
int getRightMotorSpeed(){
  return digitalRead(15) ? OCR1A * -1 : OCR1A;
}


Blackbox::Blackbox(Print& outStream): output(outStream){

}
/**
 * Writes the log header to the output stream. Call this before logging data.
 * Can be called during logging to start a new log
 */
void Blackbox::begin(){
#ifdef BLACKBOX_EVENTS_ONLY
  return;
  #endif
  output.print((const __FlashStringHelper *) blackbox_header);
  output.print(F("H Craft name: "));
  output.print(readSerialNumber(), HEX);
  output.print(" ");
  output.print(readBootCount(),DEC);
  output.print("\n");
  output.print(F("H Firmware type:Cleanflight\n"
                 "H Firmware revision:INAV 1.9.0 (dc4ef594f) SPRACINGF3\n"
                 ));
  initialized = true;
  lastLoopTime = micros();
}

/**
 * Log one frame of data.
 */
void Blackbox::log(){
#ifdef BLACKBOX_EVENTS_ONLY
  return;
    #endif
  if(!initialized){
    printString("plz call blackbox.begin() first\n");
    return;
  }

  writeByte('I');

  writeUnsigned(++loopIteration);
  uint32_t time = micros();
  writeUnsigned(time);

  writeUnsigned(time - lastLoopTime);
  lastLoopTime = time;

  writeSigned(gyro.g.x);
  writeSigned(gyro.g.y);
  writeSigned(gyro.g.z);
  writeUnsigned(rawLineSensorValues[0]);
  writeUnsigned(rawLineSensorValues[1]);
  writeUnsigned(rawLineSensorValues[2]);
  writeUnsigned(rawLineSensorValues[3]);
  writeUnsigned(rawLineSensorValues[4]);
  writeUnsigned(lineSensorValues[0]);
  writeUnsigned(lineSensorValues[1]);
  writeUnsigned(lineSensorValues[2]);
  writeUnsigned(lineSensorValues[3]);
  writeUnsigned(lineSensorValues[4]);
  writeUnsigned(linePosition);
  writeSigned(getLeftMotorSpeed());
  writeSigned(getRightMotorSpeed());
  writeSigned(compass.m.x);
  writeSigned(compass.m.y);
  writeSigned(compass.m.z);
  writeSigned(compass.a.x);
  writeSigned(compass.a.y);
  writeSigned(compass.a.z);
  writeUnsigned(frontSensorValue);
  writeUnsigned(readBatteryMillivolts());
  writeSigned(regulatorOutput.P);
  writeSigned(regulatorOutput.I);
  writeSigned(regulatorOutput.D);
  writeSigned(regulatorOutput.error);
  writeUnsigned(state);
  writeSigned(turnAngle);
  writeSigned(leftEncoder);
  writeSigned(rightEncoder);
  writeUnsigned(currentObjective);
}
/**
 * Log a new state of the state machine
 */
void Blackbox::logState(uint8_t state){
  writeByte('S');
  writeUnsigned(1 << state);//write state as flight mode
  writeUnsigned(0);//write useless state flags
}


void Blackbox::writeUnsigned(uint32_t value){
  while (value > 127) {
    writeByte((uint8_t) (value | 0x80)); // Set the high bit to mean "more bytes follow"
    value >>= 7;
  }
  writeByte((uint8_t)value);
}
void Blackbox::writeSigned(int32_t value){
  uint32_t zigzag = (uint32_t) (value << 1) ^ (value >> 31);
  writeUnsigned(zigzag);
}


/**
 * Inform the ESP32 about events like completed or failed commands and errors.
 */
void Blackbox::event(enum ZumoEvent event){
  writeByte('E');
  writeByte((uint8_t) event);
}
