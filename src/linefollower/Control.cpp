#include "Control.h"
#include "Hardware.h"
#include "Sensors.h"
#include "Regulation.h"
Regulator* activeRegulator = &turnRegulator;
enum transportState state = ON_NODE;

bool lineStateMachine(bool);
void changeRegulator(Regulator *newRegulator){
  activeRegulator->stop();
  newRegulator->init();
  activeRegulator = newRegulator;
}

/*
 *
 */
bool control() {
  if(currentObjective == STOP){//just stop, not exactly rocket science
    //changeRegulator(&dummyRegulator);
    return true;
  }else{//let's see where we are
    bool finished = activeRegulator -> run();
    return lineStateMachine(finished);
  }
}

//Determines where we currently are between the current grid nodes and chooses an appropriate regulator.
bool lineStateMachine(bool forceAdvance) {
  uint8_t aboveLines = aboveLine();

  bool currentAngle = turnAngleDeg;
  switch (state) {
  case ON_NODE:
    if(forceAdvance){
      state = LEAVING_NODE;
      changeRegulator(&lineRegulator);
    }
    break;
  case LEAVING_NODE:
    if((aboveLines & CENTER_SENSORS) == CENTER_SENSORS){
      state = BETWEEN_NODES;
    }
    break;
  case BETWEEN_NODES:
    if (!(aboveLines & CENTER_SENSORS)) {
      state = ENTERING_NODE;
      turnSensorReset();
      changeRegulator(&lineRegulator);
      //currentMaxSpeed = 50;
    }
    break;
  case ENTERING_NODE:
    if (!(aboveLines & (CENTER_SENSORS))){
      state = CROSSING_NODE_CENTERLINE;
    }
    break;
  case CROSSING_NODE_CENTERLINE:
    if ((aboveLines & CENTER_SENSORS) == CENTER_SENSORS) {
      state = ON_NODE;
      changeRegulator(&turnRegulator);
      return true;
    }
    break;
  }
  return false;
}


/*returns a bit pattern indicating which sensors are on a black line
 *e.g. 00010011 => sensors 0,1 and 4 are on a black line
 */

uint8_t aboveLine() {
  uint8_t res=0;
  for(uint8_t i = 0; i < 5; i++){
    if(lineSensorValues[i] > BLACK_LINE_THRESHOLD){
      res |= 1 << i;;
    }
  }
  return res;
}


void resetRegulatorOutput(){
  memset(&regulatorOutput, 0, sizeof(regulatorOutput));
}
