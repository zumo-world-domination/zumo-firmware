#ifndef BLACKBOX_H
#define BLACKBOX_H
#include <Print.h>
#include <stdint.h>
#include <Zumo32U4.h>
#include <Arduino.h>
#include "ZumoProtocol.h"

#define BLACKBOX_EVENTS_ONLY

#define BLACKBOX_ASK 0
#define BLACKBOX_SERIAL 1
#define BLACKBOX_USB 2

#define BLACKBOX_DEVICE BLACKBOX_SERIAL//Choose from the values defined above


/**
 * Blackbox Logger for Zumo32U4
 *
 * Initialize with output stream (e.g. Serial1) and call begin() to start writing the log. Use log() to log one frame of data. Use logState(state) to log a transition in the state machine.
 * To view the logs, use the INAV/Betaflight/Cleanflight blackbox viewer and patch js/flightlog_fielddefs.js with the correct labels for the states
 *
 * Check http://cleanflight.readthedocs.io/en/latest/development/Blackbox Internals/ for docs on the log format
 */

const char blackbox_header[] PROGMEM = "H Product:Blackbox flight data recorder by Nicholas Sherlock\n"
  "H Data version:2\n"
  "H gyro.scale:0x2f5be6ff\n" //factor used to scale the gyro values to deg/s. not entirely accurate, but good enough
  "H I interval:1\n"
  "H Field I name:loopIteration,time,loopTime,gyroADC[0],gyroADC[1],gyroADC[2],lineRaw[0],lineRaw[1],lineRaw[2],lineRaw[3],lineRaw[4],lineCalibrated[0],lineCalibrated[1],lineCalibrated[2],lineCalibrated[3],lineCalibrated[4],linePosition,motors[0],motors[1],magADC[0],magADC[1],magADC[2],accADC[0],accADC[1],accADC[2],frontDistance,vbat,PID_P,PID_I,PID_D,error,transportState,turnAngle,encoder[0],encoder[1],objective\n"
  "H Field I signed:0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1,1,1,0\n"
  "H Field I predictor:0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n"
  "H Field I encoding:1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,0,0,0,1\n"
  "H Field P predictor:0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n"
  "H Field P encoding:1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n"
  "H Field S name:flightModeFlags,stateFlags\n"//we don't need the state flags but the logs won't decode otherwise
  "H Field S signed:0,0\n"
  "H Field S predictor:0,0\n"
  "H Field S encoding:1,1\n";

class Blackbox{
 private:
  Print& output;
  bool initialized = false;
  uint32_t loopIteration=0;
  uint32_t lastLoopTime;
  void writeUnsigned(uint32_t value);
  void writeSigned(int32_t value);
  void writeByte(uint8_t value){
    while(output.availableForWrite()<1);
    output.write(value);
  };
  void printString(const char *string){
    for(uint16_t i = 0; i < strnlen(string,255);){
      i += output.print(string+i);
    }
  }

 public:
  Blackbox(Print& outStream);
  void begin();
  void log();
  void logState(uint8_t state);
  void changeOutput(Print& outStream){output = outStream;};
  void event(enum ZumoEvent event);
};
extern Blackbox blackbox;
#endif
