#ifndef SENSORS_H
#define SENSORS_H
#include "Hardware.h"
#include "LineSensor.h"
#include "TurnSensor.h"
#include "SensorFilter.h"

#define WHITE_LINE 0
#define BLACK_LINE_THRESHOLD 300
#define TURN_SENSOR_RESOLUTION 100L //defines how many steps of the turn sensor equal 1 degree of rotation

extern SensorFilter* sensorFilters[3];
extern int linePosition;
extern unsigned int lineSensorValues[];
extern unsigned int rawLineSensorValues[];
extern int32_t turnAngle;
extern int16_t turnAngleDeg;
extern int16_t turnRate;
extern unsigned int frontSensorValue;
extern int32_t leftEncoder, rightEncoder;

void initSensors();
void updateSensors();

#endif
