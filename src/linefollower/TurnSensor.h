#ifndef TURN_SENSOR_H
#define TURN_SENSOR_H
#include <stdint.h>
#include "Hardware.h"
#include "Sensors.h"
// These are defined in TurnSensor.cpp:
void turnSensorSetup();
void turnSensorReset();
void turnSensorUpdate();
int calculateAngleError(uint16_t targetAngle);


#endif
