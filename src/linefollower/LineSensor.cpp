#include <Wire.h>
#include "LineSensor.h"
#include "Sensors.h"

SensorFilter* sensorFilters[3];
unsigned int lastLinePosition;
unsigned int readLine(unsigned int *rawValues,unsigned  int *calibratedValues)
{
  unsigned char i, on_line = 0;
  unsigned long avg; // this is for the weighted total, which is long
  // before division
  unsigned int sum; // this is for the denominator which is <= 64000
  lineSensors.read(rawValues);
  lineSensors.readCalibrated(calibratedValues);

  calibratedValues[1] = sensorFilters[0]->filterSensorValue(calibratedValues[1]);
  calibratedValues[2] = sensorFilters[1]->filterSensorValue(calibratedValues[2]);
  calibratedValues[3] = sensorFilters[2]->filterSensorValue(calibratedValues[3]);

  avg = 0;
  sum = 0;

  for (i = 1; i < 4; i++) {
    int value = lineSensorValues[i];
    if (WHITE_LINE)
      value = 1000 - value;

    // keep track of whether we see the line at all
    if (value > 200) {
      on_line = 1;
    }

    // only average in values that are above a noise threshold
    if (value > 50) {
      avg += (long)(value) * (i * 1000);
      sum += value;
    }
  }

  if (!on_line){
    // If it last read to the left of center, return 0.
    if (lastLinePosition < 2000)
      return 0;

    // If it last read to the right of center, return the max.
    else
      return 4000;
  }
  lastLinePosition = avg / sum;
  return lastLinePosition;
}
