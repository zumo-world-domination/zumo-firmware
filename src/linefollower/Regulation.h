#ifndef REGULATION_H
#define REGULATION_H
#include "LineSensor.h"
#include <time.h>
#include <stdlib.h>
#include "Hardware.h"
#include "Sensors.h"

#define TURN_MIN_COMMAND 100

class Regulator{
 public:
  virtual void init(){};
  virtual bool run();
  virtual void stop(){};
};

class DummyRegulator: public Regulator{
 public:
  bool run(){return true;}
};
class LineRegulator: public Regulator{
 private:
  // Variable for the derivative part of the regulation
  int16_t lastError = 0;
 public:
  bool run();
  void init();
};


class TurnRegulator: public Regulator{
 private:
 public:
  bool run();
  void stop();
};
class ApproachRegulator: public Regulator{
 private:
 public:
  void init();
  bool run();
};

extern TurnRegulator turnRegulator;
extern LineRegulator lineRegulator;
extern ApproachRegulator approachRegulator;
extern DummyRegulator dummyRegulator;

void resetRegulatorOutput();

struct RegulatorOutput{
  int leftCmd;
  int rightCmd;
  int error;
  int P;
  int I;
  int D;
};
extern struct RegulatorOutput regulatorOutput;

/*extern uint16_t controlMaxSpeed;
extern const uint16_t MAX_SPEED;
extern const uint16_t APPROACH_STOP_SPEED;*/

extern bool reverse;

// Used by the turn function.
const int FAST_TURN_SPEED = 150; // Speed 150 is safe.
const int SLOW_TURN_SPEED = 100; // Speed 100 is safe.
const int STOP_INTERVAL = 5;




//Turning using the gyro. A positive value will result in a left turn. 
//void gyroTurn(int degrees);

//Returns how far away an object ahead is.
void handleObjectAhead();

#endif
