#ifndef LINE_SENSOR_H
#define LINE_SENSOR_H
#include "Sensors.h"
unsigned int readLine(unsigned int* calibratedValues, unsigned int* rawValues);
#endif
