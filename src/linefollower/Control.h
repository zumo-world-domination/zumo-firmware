#ifndef CONTROL_H
#define CONTROL_H
#include "Governor.h"
#include "Regulation.h"
#define LEFT_SENSOR (0b00001)
#define RIGHT_SENSOR (0b10000)
#define CENTER_SENSORS (0b01110)
bool control();//return true if objective has been fulfilled

extern Regulator *activeRegulator;

enum transportState {ON_NODE, LEAVING_NODE, BETWEEN_NODES, ENTERING_NODE, CROSSING_NODE_CENTERLINE};

extern enum transportState state;

//Determines if a given sensor intensity is considered to be above a line or not.
uint8_t aboveLine();


#endif
