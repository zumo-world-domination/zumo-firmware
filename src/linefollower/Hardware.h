#include <Zumo32U4.h>
#ifndef HARDWARE_H
#define HARDWARE_H

#define LINE_SENSORS 5
//The pin from where we can read the value of the added IR-sensor. Same pin as the buzzer
#define FRONT_SENSOR_PIN A7



extern Zumo32U4LineSensors lineSensors;
extern Zumo32U4ButtonA buttonA;
extern Zumo32U4ButtonB buttonB;
extern Zumo32U4ButtonC buttonC;
#ifdef LCD
extern Zumo32U4LCD lcd;
#endif
extern L3G gyro;
extern LSM303 compass;
extern Zumo32U4Motors motors;
extern Zumo32U4ProximitySensors proxSensors;
extern Zumo32U4Encoders encoders;
#endif
