#include "Sensors.h"

unsigned int lineSensorValues[LINE_SENSORS];
int linePosition;
unsigned int rawLineSensorValues[LINE_SENSORS];

int32_t leftEncoder, rightEncoder;
unsigned int frontSensorValue;

void calibrateSensors()
{
  // Setup the gyro.
  turnSensorSetup();
  // begin automatic sensor calibration
  // by rotating in place to sweep the sensors over the line
  int angle;
  motors.setSpeeds(-100, 100);
  do {
    turnSensorUpdate();
    angle = turnAngleDeg;
    lineSensors.calibrate();
  } while (angle < 45);
  motors.setSpeeds(100, -100);
  do {
    turnSensorUpdate();
    angle = turnAngleDeg;
    lineSensors.calibrate();
  }
  while ((angle > 315) || (angle < 60) );
  motors.setSpeeds(-100, 100);
  do {
    turnSensorUpdate();
    angle = turnAngleDeg;
    lineSensors.calibrate();
  }
  while (angle > 180);
  motors.setSpeeds(0, 0);
}


void initSensors(){
  //Make it so we can read from the pin the added IR-sensor is added to.
  pinMode(FRONT_SENSOR_PIN, INPUT);
  lineSensors.initFiveSensors();
  proxSensors.initFrontSensor(); //probably unnecesary

  for (int i = 0; i < 3; ++i){
    sensorFilters[i] = new SensorFilter(9);
  }

#ifdef LCD
  lcd.clear();
  lcd.print(F("Press A"));
  lcd.gotoXY(0, 1);
  lcd.print(F("to calib"));
  #endif
  //  buttonA.waitForButton();
  // Calibrate the gyro and the line sensors to react
  // to the color spectra encountered in the grid.
  calibrateSensors();
  compass.init();
  compass.enableDefault();

}

void updateSensors(){
  turnSensorUpdate();
  frontSensorValue = analogRead(FRONT_SENSOR_PIN);
  linePosition = readLine(rawLineSensorValues, lineSensorValues);
  compass.read();
  rightEncoder += encoders.getCountsAndResetRight();
  leftEncoder += encoders.getCountsAndResetLeft();
}
