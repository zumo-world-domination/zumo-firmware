#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H
#ifndef EOF
#define EOF -1
#endif
#include "stdint.h"
class CircularBuffer{
 private:
  uint8_t head;
  uint8_t tail;
  uint8_t size;
  uint8_t* data;
 public:
  CircularBuffer(void *memory, uint8_t length);//pass allocated memory area and length in bytes
  uint8_t put(uint8_t byte);//return 0 on success
  int get();//return byte cast to integer on success, EOF otherwise
  void flush();
  bool isFull();
  bool isEmpty();
};

#endif
