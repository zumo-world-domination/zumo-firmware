#include "Governor.h"
#ifdef GOVERNOR_BACK_FORTH
uint16_t currentMaxSpeed= 200;

enum ZumoCommand currentObjective = GO_NORTH;
enum ZumoCommand lastObjective = GO_NORTH;


enum ZumoCommand getNewObjective(){
  enum ZumoCommand newObjective;
  switch(lastObjective){
  case STOP:
    newObjective = GO_NORTH;
    break;
  case GO_NORTH:
    newObjective = GO_SOUTH;
    break;
  case GO_EAST:
    newObjective = GO_NORTH;
    break;
  case GO_SOUTH:
    newObjective = GO_NORTH;
    break;
  case GO_WEST:
    newObjective = GO_SOUTH;
    break;
  default:
    newObjective = STOP
;
    break;
  }
  lastObjective = newObjective;
  return newObjective;
}
#endif
