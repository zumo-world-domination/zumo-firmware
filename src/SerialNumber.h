#ifndef SERIAL_NUMBER_H
#define SERIAL_NUMBER_H


uint16_t readBootCount();

uint16_t readSerialNumber();

#endif
