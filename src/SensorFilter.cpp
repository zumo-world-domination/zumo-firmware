#include "SensorFilter.h"
SensorFilter::SensorFilter(int filterSize) {
  FILTER_SIZE = filterSize;
  currentValue = 0;
  head = new sensorFilterElement;

  head->value = 0;
  head->timeLeft = 0;
  head->next = NULL;

  int counter = 0;

  for (int i = 1; i< FILTER_SIZE; ++i) {
    counter++;
    auto leftTemp = head;
    while (leftTemp->next) {
      leftTemp = leftTemp->next;
    }
    leftTemp->next = new sensorFilterElement;
    leftTemp->next->value = FILTER_SIZE - i;
    leftTemp->next->next = NULL;
    leftTemp->next->timeLeft = i;
  }
}

int SensorFilter::filterSensorValue(int sensorValue) {
  auto iterator = head;

  auto newEntry = new sensorFilterElement;
  sensorFilterElement* previous = NULL;
  newEntry->value = sensorValue;
  newEntry->timeLeft = FILTER_SIZE;
  newEntry->next = NULL;

  int counter = 0;
  bool inserted = false;
  bool removed = false;
  int returnValue = -1;
  while (iterator) {
    if (iterator->timeLeft == 0) {
      if (counter == 0) {
        head = iterator->next;
        delete iterator;
        iterator = head;
        
      }
      else if (counter != FILTER_SIZE - 1) {
        auto temp = iterator;
        iterator = temp->next;
        delete temp;
        previous->next = iterator;
        
      }
      else {
        previous->next = NULL;
        delete iterator;
        break; //Don't like to use break, but seems like the cleanest solution
      }
    }
    if (iterator->value < sensorValue && !inserted) {
      if (counter == 0) {
        newEntry->next = head;
        head = newEntry;
      }
      else {
        newEntry->next = iterator;
        previous->next = newEntry;
      }
      inserted = true;

      newEntry->timeLeft--;
    }
    if (counter == FILTER_SIZE / 2) {
      returnValue = iterator->value;
    }

    ++counter;
    --iterator->timeLeft;
    previous = iterator;
    iterator = iterator->next;
  }
  if (!inserted) {
    previous->next = newEntry;
    --newEntry->timeLeft;
  }

  currentValue = returnValue;
  return returnValue;
}



void SensorFilter::reset() {
  auto iterator = head;
  while (iterator) {
    iterator->value = 0;
    iterator = iterator->next;
  }
}

