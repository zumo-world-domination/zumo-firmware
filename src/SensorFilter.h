#include <stdlib.h>

class SensorFilter {
private:
  int FILTER_SIZE;

  struct sensorFilterElement {
    sensorFilterElement* next;
    int value;
    int timeLeft;
  };

public:
  
  sensorFilterElement* head;
  int currentValue;

  SensorFilter(int filterSize = 9);

  int filterSensorValue(int sensorValue);
  void print();
  void reset();
  

};
