#include "Governor.h"
#include <Arduino.h>
#ifdef GOVERNOR_RANDOM
#define GRID_SIZE_NORTH_SOUTH 3
#define GRID_SIZE_EAST_WEST 2
uint8_t pos_ns = 0;
uint8_t pos_ew = 0;

enum ZumoCommand currentObjective = GO_NORTH;
enum ZumoCommand lastObjective = GO_NORTH;

uint16_t currentMaxSpeed = 200;



enum ZumoCommand getNewObjective(){
  switch(lastObjective){
  case GO_NORTH:
    pos_ns+=1;
    break;
  case GO_SOUTH:
    pos_ns-=1;
    break;
  case GO_EAST:
    pos_ew+=1;
    break;
  case GO_WEST:
    pos_ew-=1;
    break;
  }

  enum ZumoCommand newObjective;
  bool decided = false;
  do{
    int rand = random(0,4);
    switch(rand){
    case 0:
      newObjective = GO_NORTH;
      break;
    case 1:
      newObjective = GO_EAST;
      break;
    case 2:
      newObjective = GO_SOUTH;
      break;
    case 3:
      newObjective = GO_WEST;
      break;
    }
    if(!(((newObjective == GO_NORTH) && (pos_ns == (GRID_SIZE_NORTH_SOUTH-1)))||
         ((newObjective == GO_SOUTH) && (pos_ns == (0)))||
         ((newObjective == GO_EAST) && (pos_ew == (GRID_SIZE_EAST_WEST-1)))||
         ((newObjective == GO_WEST) && (pos_ew == (0))))){
      decided = true;
    }
  }while(!decided);
  lastObjective = newObjective;
  return newObjective;
}

#endif
