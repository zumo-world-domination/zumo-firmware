#include "TurnSensor.h"
#include <Wire.h>
/* Turnsensor.h and TurnSensor.cpp provide functions for
   configuring the L3GD20H gyro, calibrating it, and using it to
   measure how much the robot has turned about its Z axis. */

/* turnAngle is a 32-bit unsigned integer representing the amount
the robot has turned since the last time turnSensorReset was
called.  This is computed solely using the Z axis of the gyro, so
it could be inaccurate if the robot is rotated about the X or Y
axes.
*/
int32_t turnAngle = 0;
int16_t turnAngleDeg = 0;
// turnRate is the current angular rate of the gyro, in units of
// 0.07 degrees per second.
int16_t turnRate;

// This is the average reading obtained from the gyro's Z axis
// during calibration.
int16_t gyroOffset;

// This variable helps us keep track of how much time has passed
// between readings of the gyro.
uint16_t gyroLastUpdate = 0;

/* This should be called in setup() to enable and calibrate the
gyro.  It uses the LCD, yellow LED, and button A.  While the LCD
is displaying "Gyro cal", you should be careful to hold the robot
still.

The digital zero-rate level of the L3GD20H gyro can be as high as
25 degrees per second, and this calibration helps us correct for
that. */
void turnSensorSetup()
{
  Wire.begin();
  gyro.init();

  // 800 Hz output data rate,
  // low-pass filter cutoff 100 Hz
  gyro.writeReg(L3G::CTRL1, 0b11111111);

  // 2000 dps full scale
  gyro.writeReg(L3G::CTRL4, 0b00100000);

  // High-pass filter disabled
  gyro.writeReg(L3G::CTRL5, 0b00000000);
#ifdef LCD
  lcd.clear();
  lcd.print(F("Gyro cal"));
#endif
  // Turn on the yellow LED in case the LCD is not available.
  ledYellow(1);

  // Delay to give the user time to remove their finger.
  delay(500);

  // Calibrate the gyro.
  int32_t total = 0;
  for (uint16_t i = 0; i < 1024; i++)
  {
    // Wait for new data to be available, then read it.
    while(!gyro.readReg(L3G::STATUS_REG) & 0x08);
    gyro.read();

    // Add the Z axis reading to the total.
    total += gyro.g.z;
  }
  ledYellow(0);
  gyroOffset = total / 1024;

  // Display the angle (in degrees from -180 to 180) until the
  // user presses A.
#ifdef LCD
  lcd.clear();
#endif
  turnSensorReset();
  /*while (!buttonA.getSingleDebouncedRelease())
  {
    turnSensorUpdate();
    lcd.gotoXY(0, 0);
    lcd.print((((int32_t)turnAngle >> 16) * 360) >> 16);
    lcd.print(F("   "));
  }
  lcd.clear();*/
}

// This should be called to set the starting point for measuring
// a turn.  After calling this, turnAngle will be 0.
void turnSensorReset()
{
  gyroLastUpdate = micros();
  if(turnAngleDeg < 45 || turnAngleDeg >= 315){
    turnAngle = 0;
  }else if(turnAngleDeg >= 45 && turnAngleDeg < 135){
    turnAngle = 90*TURN_SENSOR_RESOLUTION;
  }else if(turnAngleDeg >= 135 && turnAngleDeg < 225){
    turnAngle = 180*TURN_SENSOR_RESOLUTION;
  }else{
    turnAngle = 270*TURN_SENSOR_RESOLUTION;
  }
}
const int32_t turnAngle45 = 0x20000000; // This constant represents a turn of 90 degrees.
const int32_t turnAngle90 = turnAngle45 * 2; // This constant represents a turn of approximately 1 degree.
const int32_t turnAngle1 = (turnAngle45 + 22) / (45*TURN_SENSOR_RESOLUTION);

// Read the gyro and update the angle.  This should be called as
// frequently as possible while using the gyro to do turns.
void turnSensorUpdate()
{
  // Read the measurements from the gyro.
  gyro.read();
  turnRate = gyro.g.z - gyroOffset;

  // Figure out how much time has passed since the last update (dt)
  uint16_t m = micros();
  uint16_t dt = m - gyroLastUpdate;
  gyroLastUpdate = m;

  // Multiply dt by turnRate in order to get an estimation of how
  // much the robot has turned since the last update.
  // (angular change = angular velocity * time)
  int32_t d = (int32_t)turnRate * dt;

  // The units of d are gyro digits times microseconds.  We need
  // to convert those to the units of turnAngle, where 2^29 units
  // represents 45 degrees.  The conversion from gyro digits to
  // degrees per second (dps) is determined by the sensitivity of
  // the gyro: 0.07 degrees per second per digit.
  //
  // (0.07 dps/digit) * (1/1000000 s/us) * (2^29/45 unit/degree)
  // = 14680064/17578125 unit/(digit*us)
  turnAngle += ((int64_t)d * 14680064 / 17578125 ) / turnAngle1;
  if(turnAngle / TURN_SENSOR_RESOLUTION > 360){
    turnAngle -= 360L * TURN_SENSOR_RESOLUTION;
  }
  if(turnAngle / TURN_SENSOR_RESOLUTION < 0){
    turnAngle += 360L * TURN_SENSOR_RESOLUTION;
  }
  turnAngleDeg = turnAngle/TURN_SENSOR_RESOLUTION;
}

int calculateAngleError(uint16_t targetAngle){
  uint16_t currentAngle = turnAngleDeg;
  int angleError = currentAngle - targetAngle;
  if(angleError > 180){
    angleError -= 360;
  }else if(angleError < -180){
    angleError += 360;
  }
  return angleError;
}
