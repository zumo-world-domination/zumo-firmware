#include "Regulation.h"
#include "Governor.h"
#include "Control.h"


DummyRegulator dummyRegulator;
TurnRegulator turnRegulator;
LineRegulator lineRegulator;
ApproachRegulator approachRegulator;

struct RegulatorOutput regulatorOutput;

bool LineRegulator::run(){
  regulatorOutput.error = lineSensorValues[0] - lineSensorValues[4];

  regulatorOutput.P = regulatorOutput.error / 4;
  regulatorOutput.D =  (regulatorOutput.error - lastError)/4;
  int16_t speedDifference = regulatorOutput.P + regulatorOutput.D;

  lastError = regulatorOutput.error;

  // Get individual motor speeds.  The sign of speedDifference
  // determines if the robot turns left or right.
  int leftSpeed = (int16_t)currentMaxSpeed + speedDifference;
  int rightSpeed = (int16_t)currentMaxSpeed - speedDifference;

  // Constrain our motor speeds to be between 0 and maxSpeed.
  // One motor will always be turning at maxSpeed, and the other
  // will be at maxSpeed-|speedDifference| if that is positive,
  // else it will be stationary.  For some applications, you
  // might want to allow the motor speed to go negative so that
  // it can spin in reverse.
  leftSpeed = constrain(leftSpeed, 0, (int16_t)currentMaxSpeed);
  rightSpeed = constrain(rightSpeed, 0, (int16_t)currentMaxSpeed);

  regulatorOutput.rightCmd = rightSpeed;
  regulatorOutput.leftCmd = leftSpeed;
  return false;
}

void LineRegulator::init(){
  lastError = 0;
}

void TurnRegulator::stop(){
  //turnSensorReset();
}
bool TurnRegulator::run(){
  int targetAngle = 0;
  switch(currentObjective){
  case GO_NORTH:
    targetAngle = 0;
    break;
  case GO_EAST:
    targetAngle = 270;
    break;
  case GO_SOUTH:
    targetAngle = 180;
    break;
  case GO_WEST:
    targetAngle = 90;
    break;
  }
  int error = calculateAngleError(targetAngle);
  regulatorOutput.error = error;
  regulatorOutput.rightCmd = -1*constrain(error + (error < 0?-TURN_MIN_COMMAND:TURN_MIN_COMMAND), -150, 150);
  regulatorOutput.leftCmd = constrain(error + (error < 0 ? -TURN_MIN_COMMAND:TURN_MIN_COMMAND), -150, 150);
  return abs(error) < 5;
}

void ApproachRegulator::init(){

}

bool ApproachRegulator::run(){

}
//Adjusts the speed of the robot based on objects ahead.

int frontSensorValues[3] = {200,201,202};
void handleObjectAhead(){
  int closest = 0;
  if(frontSensorValues[0] > frontSensorValues[1]){
    if(frontSensorValues[0] > frontSensorValues[2]){
      closest = frontSensorValues[0];
    }
    else{
      closest = frontSensorValues[2];
    }
  }else{
    if(frontSensorValues[1] > frontSensorValues[2]){
      closest = frontSensorValues[1];
    }
    else{
      closest = frontSensorValues[2];
    }
  }
  int distance = 500 - closest;
  if (distance > 200){ //200 is the current MAX_SPEED
    currentMaxSpeed = 200;
  }
  else{
    currentMaxSpeed = distance;
  }
}

