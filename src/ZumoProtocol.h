#ifndef ZUMO_PROTO_H
#define ZUMO_PROTO_H

enum ZumoEvent{HALT_AND_CATCH_FIRE='H', COMMAND_COMPLETE='K', COMMAND_ABORTED='A'}__attribute__((packed));
enum ZumoCommand {STOP='P', GO_NORTH = 'N', GO_SOUTH = 'S', GO_EAST = 'E', GO_WEST = 'W', ZUMO_RESET='R'}__attribute__((packed));

#endif
