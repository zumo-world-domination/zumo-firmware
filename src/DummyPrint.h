#include <Arduino.h>
#ifndef DUMMY_PRINT_H
#define DUMMY_PRINT_H
class DummyPrint: public Print{
 public:
  virtual size_t write(uint8_t soos){return 1;};
  virtual size_t write(const uint8_t *buffer, size_t size){return size;};
};
#endif
